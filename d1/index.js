console.log("hello Alpha");

//Synchronous vs Asynchronous


// consolo.log("hello from");
// console.log("I'll rub if no error accurs!");

//When certain statements take a lot of time to process, this slows down our code when  c

// for(let i = 0; i<= 150; i++){
// 	console.log(i);
// }

// console.log('Hello again'); 


//API Application Programming Interface

//FETCH API
	//pending:
	//fulfilled:
	//rejected:

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))



fetch('https://jsonplaceholder.typicode.com/posts')
.then(res=> console.log(res.status))

fetch('https://jsonplaceholder.typicode.com/posts')
.then((res)=>res.json())
.then((data)=>{
	console.log(data)
	console.log('This will run after the promise has been fulfilled')
})
console.log("This will run first")

async function fetchData(){

	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	console.log(result)
	console.log(typeof result)

	console.log(result.body)
	let json = await result.json();

	console.log(json)
}
fetchData();

//GET a specific post


fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((res)=> res.json())
.then((data)=>console.log(data));

//Create a post

fetch('https://jsonplaceholder.typicode.com/posts',{
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New posts',
		body: "Hello World",
		user_Id: 1,
	})
})
.then((res)=>res.json())
.then((data)=>console.log(data));

//updating a post

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: "PUT",
	header: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'New posts',
		body: "Hello Again",
		user_Id: 1
	})
})
.then((res)=>res.json())
.then((data)=>console.log(data))


//DELETE post

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: "DELETE"

})
.then((res)=> res.json())
.then((data)=>console.log(data));

/*
mini-activity
*/
//1.
fetch('http://jsonplaceholder.typicode.com/todos')
.then((res)=> res.json())
.then((data)=>console.log(data));
//2.

fetch('http://jsonplaceholder.typicode.com/todos')
.then((res)=> res.json())
.then((data)=>{
	let list = data.map((todo=>{
		return todo.title;
	}))
	console.log(list)
});