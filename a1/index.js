console.log("Hello ALpha")
console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

//1.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((res)=>res.json())
.then((data)=>{
	let list = data.map((todo=>{
		return todo.title
	}))
	console.log(list)
})

//2.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((res)=>res.json())
.then((data)=>{
	let title = data.title
	let status = data.completed
	console.log(`The item "${title}" on the list has a status of ${status}. `)
})

//3.
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: "POST",
	headers:{
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: 'Created todo list item',
		completed: false,
		userId: 1
	})
})
.then((res)=>res.json())
.then((data)=>console.log(data))
//4.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PUT",
	headers:{
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item ',
		description: 'To update th my todo List with a different data stucture.',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	})
})
.then((res)=>res.json())
.then((data)=>console.log(data))

//5.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PATCH",
	headers:{
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		status: 'completed',
		dateCompleted: '07/09/21'
	})
})
.then((res)=>res.json())
.then((data)=>console.log(data))

//6.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "DELETE"

})
.then((res)=>res.json())
.then((data)=>console.log(data))


//A. https://jsonplaceholder.typicode.com/todos

//B. https://jsonplaceholder.typicode.com/todos/1
//C. https://jsonplaceholder.typicode.com/todos 
/*
{
    "title": "My first Blog post.",
    "body": "Hello world",
    "userId": 1,
    "id": 201
}
*/

//D. https://jsonplaceholder.typicode.com/todos

//E.https://jsonplaceholder.typicode.com/todos/1 
